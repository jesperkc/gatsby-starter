/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const path = require('path');
const slash = require('slash');
const { slugs } = require("./src/helpers/slugs");

/*
`
  query {
    studies: allPrismicCases {
      nodes {
        id
        lang
        uid
      }
    }
    plain: allPrismicPlainTextPage  {
      nodes {
        id
        lang
        uid
      }
    }
    pages: allPrismicPage {
      nodes {
        id
        lang
        uid
      }
    }
  }
`
 */
let gql = `
    {
      pages: allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___date] }
        limit: 1000
      ) {
        nodes {
          frontmatter {
            path
            slug
            lang
          }
        }
      }
    }
`;

const pageTemplate = path.resolve("src/templates/page-template.js");
const errorTemplate = path.resolve("src/templates/404-template.js")

const createPagesMethod = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators;
  return new Promise((resolve, reject) => {

    resolve(
      graphql(gql).then(result => {
        if (result.errors) {
          reject(result.errors);
        }

        result.data.pages.nodes.forEach(edge => {
          createPage(CreatePage(getPageData(edge, pageTemplate)));
        });

        createPage(CreateError('en-gb'));

        return;
      })
    );

    resolve();
  });
};

const getPageData = (data, template) => {
  return {
    path: data.frontmatter.path,
    slug: data.frontmatter.slug,
    lang: data.frontmatter.lang,
    template: template,
  }
}

const CreatePage = ({path, slug, lang, template}) => {
  // let slug = frontmatter.uid ? frontmatter.uid : null;
  // let lang = frontmatter.lang ? frontmatter.lang : null;
  // let path = slugs.nav(slug, lang);
  if (slug === 'index') path = path.replace('index', '');
  console.log(path, slug, lang);
  return {
    path: `${path}`,
    component: slash(template),
    context: {
      slug: `${slug}`,
      lang: `${lang}`,
    },
  };
};

const CreateError = (lang) => {
  let slug = '404';
  let path = slugs.nav(slug, lang);

  return {
    path: `${path}`,
    component: slash(errorTemplate),
    context: {
      slug: `${slug}`,
      lang: `${lang}`,
    },
  };
};




exports.createPages = createPagesMethod;