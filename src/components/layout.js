/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */
import styled from "@emotion/styled";
import { graphql, useStaticQuery } from "gatsby";
import PropTypes from "prop-types";
import React from "react";
import Footer from "./footer/footer";
import Header from "./header/header";
import { fonts, responsiveFont, responsiveWidth, settings } from './grid/settings';
import "./layout.css";


const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    <LayoutWrapper>
      <Header />
      {children}
      <Footer/>
    </LayoutWrapper>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout



  /* @font-face {
    font-family: 'Karimoku';
    font-weight: normal;
    font-display: auto;
    src: url(${KarimokuSansFont}) format('woff2'), 
      url('/fonts/KarimokuSans/KarimokuSans-Regular.woff') format('woff');
  } */

const LayoutWrapper = styled.div`




  body & {
    font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
    ${responsiveFont(settings.typo.p)};
  }

  
  white-space: pre-line;
  @media (max-width: ${settings.mobileWidth - 1}px){
    white-space: normal;
  }

  p{
    
    padding: 0;
    &:empty{
      display: none;
    }
    &:first-of-type{
      margin-top: 0;
    }
    &:last-of-type{
      margin-bottom: 0;
    }
  }
  h1{
  }
  h2{
  }
  h3{
  }
  h5{
  }
  p + h3{
    margin-top: 5.5vw;
  }
  

  .clipper {
    position: absolute;
    width: 100%;
    top: 0;
    bottom: 0;
    clip: rect(0, auto, auto, 0);
  }

  .headroom {
    top: 0;
    left: 0;
    right: 0;
  }
  .headroom-wrapper{
    position: absolute;
    min-width: 100%;
    z-index: 900;
  }
  .headroom {
    top: 0;
    left: 0;
    right: 0;
    z-index: 1;
    transition: transform 200ms ease-in-out;
  }
  .headroom--unfixed {
    position: fixed;
    transform: translateY(0);
  }
  .headroom--scrolled {
  }
  .headroom--unpinned {
    position: fixed;
    transform: translateY(-100%);
  }
  .headroom--pinned {
    position: fixed;
    transform: translateY(0%);
  }


  .default-fade{
    opacity: 0;
    transition: opacity 1000ms;
    will-change: opacity;
  }

  .tl-wrapper-status--exiting .default-fade{
    opacity: 0;
  }
  .tl-wrapper-status--entered .default-fade{
    opacity: 1;
  }
`;