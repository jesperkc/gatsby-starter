import { css } from "@emotion/core";
import styled from "@emotion/styled";
import Link from 'gatsby-link';
import React from "react";
import { Container, Row, Col, Spacer } from '../grid/grid.component';
import { colors, responsiveFont, settings } from '../grid/settings';
import { external_link, H6 } from '../grid/typography.component';
import { ReactComponent as Logo } from '../../../static/images/logo.svg';
const { slugs } = require("../../helpers/slugs");
const { linkify } = require('../../helpers/helpers');

class FooterComponent extends React.Component {

  constructor() {
    super();

  }

  render() {
    const {
      lang,
      footer_text_signup_for_newsletter,
      footer_placeholder_signup,
      footer_links_label,
      footer_links,
      footer_social_media_label,
      footer_social_media_links,
      footer_language_label,
      footer_language_english_text,
      footer_language_japanese_text,
      footer_presskit_label,
      footer_presskit_link,
      footer_presskit_link_text,
      footer_contact_label,
      footer_contact_email,

    } = this.props;

    // console.log(mailchimpmsg);
    return (
      <div>
        <FooterTopWrapper>
          <Container>
            <Row wrap={'wrap'}>
              <Col css={cell_css}>
                <Logo/>

                <br /><br /><br /><br />

                {footer_text_signup_for_newsletter}<br /><br />
                <br /><br /><br /><br />
              </Col>
              <Col />
              <Col css={cell_css}>
                <Faded>{footer_links_label}</Faded><br />
                <ul>
                  {footer_links && footer_links.map(link => (
                    <li key={link.text}><Link to={slugs.nav(link.link && link.link.uid, lang)}><span>{link.text}</span></Link></li>
                  ))}
                </ul>

                <br /><br />
              </Col>
              <Col css={cell_css}>
                <Faded>{footer_social_media_label}</Faded><br />
                <ul>
                  {footer_social_media_links && footer_social_media_links.map(link => (
                    <li key={link.text}><a href={`${link.link.url}`} target="_blank" rel="noopener noreferrer"><span>{link.text}</span></a></li>
                  ))}
                </ul>
                {/* <br />
                <br />
                <Faded>{footer_language_label}</Faded><br />
                {footer_language_english_text} | {footer_language_japanese_text} */}
                <br /><br /><br /><br />
              </Col>
              <Col css={cell_css}>
                {footer_presskit_link &&
                  <>
                    <Faded>{footer_presskit_label}</Faded><br />
                    <a href={footer_presskit_link.url} css={external_link} target="_blank" rel="noopener noreferrer">{footer_presskit_link_text}</a><br />
                    <br /><br />
                  </>
                }
                {footer_contact_email &&
                  <>
                    <Faded>{footer_contact_label}</Faded><br />
                    <a href={footer_contact_email.url} css={external_link}>Email us</a>
                  </>
                }
              </Col>
            </Row>

          </Container>
        </FooterTopWrapper>

      </div>
    )
  }
}

export default FooterComponent



const FooterTopWrapper = styled.div`

  background-color: ${colors.beigedark};
  padding: 50px 0;

  ${responsiveFont(settings.typo.small)};
  line-height: 1.25;

  input {
    width: 70%;
    float: left;
  }

  ul{
    margin: 0;
    padding: 0;
    list-style: none;
  }
  li{
    margin-bottom: 1.3em;
  }
  a{
    white-space: nowrap;
  }
`;

const Faded = styled.div`
  opacity: .5;
`;
const cell_css = css`
  min-width: 190px;
`;
