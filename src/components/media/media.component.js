import anime from 'animejs';
import Image from 'gatsby-image';
import React from 'react';
import styled from "@emotion/styled";
import { css } from "@emotion/core";
// import Lottie from 'react-lottie';
import TrackVisibility from 'react-on-screen';
import eventsService from '../../utils/events.service';

class MediaComponent extends React.Component {
  render() {
    const { image, video, loop, backgroundColor, textOverlay, showFullscreenControl = false, showMuteControl = false, onLoad = () => { return; }, autoplay = false, aspectRatio, alt = '', title = '' } = this.props;
    let visible = this.props.visible;
    if (visible == null) {
      visible = false;
    }
    if (video && autoplay == false) video.autoplay = false;
    if (image && autoplay == false) image.autoplay = false;

    return renderMedia(image, video, loop, backgroundColor, visible, showFullscreenControl, showMuteControl, onLoad, aspectRatio, textOverlay, alt, title);
  }
}

const getSVG = (image) => {
  //console.log(image);
  return image.inlineSvg
}

const renderMedia = (image, video, loop, backgroundColor, visible, showFullscreenControl, showMuteControl, onLoad, aspectRatio, textOverlay, alt, title) => {
  // if (video && video.localVideo) {
  //     return (
  //         <TrackVisibility partialVisibility className={visibility_css}>
  //             <VideoMediaComponent video={video} loop={loop} />
  //         </TrackVisibility>
  //     );
  // }
  console.log('image',image);
  if (image) {
    if (image.format === 'svg') {
      onLoad();
      return (
        image.url ? <img src={image.url} id="Media-SVG" className={'media media-svg'} /> :
          <InlineSVG
            id="Media-SVG"
            dangerouslySetInnerHTML={{
              __html: getSVG(image),
            }}
          />
      );
    }
    if (image.format === 'json') {
      onLoad();
      return (
        <TrackVisibility partialVisibility className={'media media-lottie'}>
          <LottieMediaComponent
            image={image}
            loop={loop}
            visible={visible}
          />
        </TrackVisibility>
      );
    }
    if (image.format === 'mp4') {
      return (
        <TrackVisibility partialVisibility className={['media media-video', visibility_css].join(' ')}>
          <VideoMediaComponent
            video={image}
            loop={loop}
            visible={visible}
            textOverlay={textOverlay}
            showFullscreenControl={showFullscreenControl}
            showMuteControl={showMuteControl}
            onLoad={onLoad}
            aspectRatio={aspectRatio}
          />
        </TrackVisibility>
      );
    }
    if (image.format === 'gif') {
      image.sizes.src = image.url;
      delete image.sizes.srcSet;
      return (
        <Image
          {...image}
          id="Media-Image"
          fadeIn={false}
          backgroundColor={backgroundColor}
          className={['media media-image', image_css].join(' ')}
          nopin="nopin"
          onLoad={onLoad}
          alt={alt}
          title={title}
        />
      );
    }
    return (
      <Image
        {...image}
        id="Media-Image"
        fadeIn={false}
        backgroundColor={backgroundColor}
        className={['media media-image', image_css].join(' ')}
        nopin="nopin"
        onLoad={onLoad}
      />
    );
  }
  return <div />;
};

class VideoMediaComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      autoPlay: false,
      localVideo: props.video.url,
      loop: props.loop,
      play: props.visible,
      visible: props.visible,
      textOverlay: props.textOverlay,
      menuOpen: false,
      unmutetext: 'Unmute',
      linkedin: false,
    };

    //console.log("VideoMediaComponent ", props.visible);
    this.onVisibleChange = this.onVisibleChange.bind(this);
    this.playPause = this.playPause.bind(this);
    this.unmute = this.unmute.bind(this);
    this.fullScreen = this.fullScreen.bind(this);
    this.onMenuOpen = this.onMenuOpen.bind(this);
  }
  componentDidMount() {
    var userAgent = window.navigator.userAgent.toLowerCase();
    this.setState({ linkedin: /Mobile\/16C101/.test(userAgent) });

    this.media.setAttribute('webkit-playsinline', '');

    eventsService.listenEvent('menuOpen', 'footer', this.onMenuOpen);
  }
  componentWillReceiveProps(props) {
    //console.log("props.isVisible", props.isVisible);
    this.onVisibleChange(props.isVisible);
  }
  onMenuOpen(menuOpen) {
    //console.log("onMenuOpen", menuOpen);
    this.setState({ menuOpen: menuOpen }, this.playPause);
  }
  onVisibleChange(visible) {
    //console.trace();
    this.setState({ visible: visible }, this.playPause);
  }
  playPause() {
    //console.log("playPause", this.state.visible, this.state.menuOpen, this.state.localVideo);

    if (this.state.linkedin === true) {
      this.media.currentTime = 0;
      this.media.pause();
    } else {
      if (this.media) {
        if (this.state.visible == true && this.state.menuOpen == false) {
          this.play();
        } else {
          this.pause();
        }
      }
    }
  }
  play() {
    this.media.volume = 1;
    var promise = this.media.play();
    if (promise !== undefined) {
      promise.then(() => {
        // Autoplay started!
      }).catch((error) => {
        // Autoplay was prevented.
        // Show a "Play" button so that user can start playback.
      });
    }
  }
  pause() {
    var myObject = {
      volume: this.media.volume * 10
    }
    anime({
      targets: myObject,
      volume: 0,
      easing: 'linear',
      round: 1,
      duration: 500,
      update: () => {
        if (this.media) {
          this.media.volume = myObject.volume / 10;
        }
      },
      complete: () => {
        if (this.media) {
          this.media.pause()
        }
      }
    })
  }
  unmute(e) {
    this.media.muted = !this.media.muted;
    this.setState({ unmutetext: this.media.muted ? 'Unmute' : 'Mute' })
    e.stopPropagation();
  }
  updateMuteText() {
    this.setState({ unmutetext: this.media.muted ? 'Unmute' : 'Mute' })
  }
  fullScreen() {

    if (this.media.requestFullscreen) {
      this.media.requestFullscreen();
    } else if (this.media.mozRequestFullScreen) {
      this.media.mozRequestFullScreen();
    } else if (this.media.webkitEnterFullScreen) {
      this.media.webkitEnterFullScreen();
    }
    this.media.muted = false;
    this.play();
    this.updateMuteText();
  }
  render() {




    let controls = false;
    if (this.state.linkedin === true) controls = true;

    return (
      <VideoWrapper aspectRatio={this.props.aspectRatio}>

        <VideoControls>
          {this.props.showFullscreenControl && <button onClick={this.fullScreen}
            data-cursor="focus"
            data-cursor-persistance="500" className='text2'>Fullscreen</button>}
          {this.props.showMuteControl && <button onClick={this.unmute} data-cursor="focus"
            data-cursor-persistance="500" className='text2'>{this.state.unmutetext}</button>}
        </VideoControls>
        <video
          ref={el => (this.media = el)}
          className={video_css}
          autoPlay={this.state.autoPlay}
          onProgress={this.onProgress}
          loop={this.state.loop}
          onPlay={() => {
            this.props.onLoad();
          }}
          muted
          playsInline
          controls={controls}>
          <source src={this.state.localVideo} type="video/mp4" />
        </video>
        <TextOverlay>{this.state.textOverlay}</TextOverlay>
      </VideoWrapper>
    );
  }
}

const VideoWrapper = styled.div`
  width: 100%;
  height: 100%;
`;

class LottieMediaComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      paused: !props.visible,
      loop: props.loop,
      jsonData: JSON.parse(props.image.jsonData),
    };

    this.animationComplete = false;

    this.onVisibleChange = this.onVisibleChange.bind(this);
  }
  componentWillReceiveProps(props) {
    this.onVisibleChange(props.isVisible);
  }
  onVisibleChange(visible) {
    this.setState({ paused: !visible });
  }
  render() {
    return (
      <LottieContainer>
        {/* <Lottie
          id="Media-Lottie"
          options={{
            loop: this.state.loop,
            autoplay: false,
            animationData: this.state.jsonData,
            rendererSettings: {
              preserveAspectRatio: 'xMidYMid',
            },
          }}
          width={'auto'}
          height={'auto'}
          isStopped={false}
          isPaused={this.state.paused}
        /> */}
        </LottieContainer>
    );
  }
}

const visibility_css = css`
    height: 100%;
    position: relative;
`;
const video_css = css`
    width: 100%;
    max-width: 100%;
`;
const TextOverlay = styled.p`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  mix-blend-mode: difference;
  padding: 1vw .65vw;
`;
const VideoControls = styled.div`
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 2;
    text-align: center;
    
    button{
      border: 0;
      background: none;
      padding: 1vw .65vw;
      margin: 0;

      color: #fff;
      min-width: 90px;
      &:focus{
        outline: none;
      }
    }
`;

const LottieContainer = styled.div`
    text-align: center;
    svg{
      width: auto !important;
      height: auto !important;
      max-width: 100%;
    }
`;

const image_css = css`
    width: 100%;
    .gatsby-image-outer-wrapper {
        width: 100%;
    }
`;

const InlineSVG = styled.div`
    max-width: 100%;
    width: 100%;
    height: 100%;
    svg {
        overflow: initial;
        max-width: 100%;
        height: auto;
        width: auto;
    }
`;

export default MediaComponent;
