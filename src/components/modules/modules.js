

import React from "react";
import SpacerComponent from '../contents/spacer';
import FooterComponent from '../footer/footer';
import HeaderComponent from '../header/header';
import HeroComponent from '../hero/hero.component';

const Modules = ({ __typename, moduledata, alldata, transitionStatus, productGroup, casestudies, categories, words, lang, nav, footer, designers }) => {

  switch (__typename) {
    case 'SpacerComponent': return <SpacerComponent transitionStatus={transitionStatus} />;
    case 'HeaderComponent': return <HeaderComponent transitionStatus={transitionStatus} />;
    case 'FooterComponent': return <FooterComponent transitionStatus={transitionStatus} />;
    case 'HeroComponent': return <HeroComponent transitionStatus={transitionStatus} />;
    default: console.log(__typename); return '';
  }
}

export default Modules;

