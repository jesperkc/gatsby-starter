// import 'pure-react-carousel/dist/react-carousel.es.css';
import React from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import styled from "@emotion/styled";
import { css, cx } from "@emotion/core";
import { Container, Row, Col, Spacer, OverflowHidden } from '../grid/grid.component';
import MediaComponent from '../media/media.component';




class CarouselComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentSlide: 0,
    };

    this.next = this.next.bind(this);
    this.clickNext = this.clickNext.bind(this);
    this.prev = this.prev.bind(this);
    this.clickPrev = this.clickPrev.bind(this);

    this.carousel = React.createRef();
    // this.visibilityChange = this.visibilityChange.bind(this);
  }
  clickNext(){
    console.log(this.carousel);
    this.carousel.current.slideNext()
  }
  clickPrev(){
    this.carousel.current.slidePrev()
  }
  next() {
    this.setState({
      currentSlide: this.state.currentSlide + 1,
    });
  }

  prev() {
    this.setState({
      currentSlide: this.state.currentSlide - 1,
    });
  }
  galleryItems() {
    let imageBackgroundColor = this.props.imageBackgroundColor;
    if (!this.props.images) {
      return [<div></div>]
    }
    console.log(this.props.images);
    return this.props.images.map((image, i) => (
      <Slide key={`key-${i}`}>
        <SlideContainer>
          <MediaComponent
            image={image.image.childImageSharp}
            loop={true}
            backgroundColor={
              imageBackgroundColor && imageBackgroundColor.hex
            }
          />
          <Text>{image.title}</Text>
        </SlideContainer>
      </Slide>
    ));
  }

  // visibilityChange(visible) {
  //   console.log('visibilityChange', visible);
  //   if (visible == true) {
  //     this.carousel._slideNext();
  //   }
  // }
  render() {
    const items = this.galleryItems();
    const settings = {
      items: items,
      autoPlay: false,
      buttonsDisabled: true,
      dotsDisabled: true,
      startIndex: 1,
      mouseDragEnabled: true,
      infinite: true,
      autoPlayActionDisabled: true,
    };
    const { backgroundColor, textColor } = this.props;
    return (
      <OverflowHidden>
      <Container>
          <Row>
            <CarouselWrapper>
              <CarouselButton
                onClick={this.clickPrev}
                className={prev_button_css}
              />
              <CarouselButton
                onClick={this.clickNext}
                className={next_button_css}
              />
              <AliceCarousel
                {...settings}
                ref={this.carousel}
                slideToIndex={this.state.currentSlide}
                onSlideChanged={this.onSlideChanged}
                className={carousel_css}
              />
            </CarouselWrapper>
          </Row>
      </Container>
      </OverflowHidden>
    );
  }
}


const carouselWidth = `70%`;
const padding = `${100 / 18 * .5}vw`;

const CarouselWrapper = styled.div`
    width: ${carouselWidth};
    margin: 0 auto;
    position: relative;
    .alice-carousel {
        padding: 0;
    }
    .alice-carousel__wrapper {
        overflow: visible;
        border: 0;
    }
    .alice-carousel__stage-item {
        /* max-height: 100vh; */
    }
`;

const CarouselButton = styled.div`
    position: absolute;
    top: 0;
    z-index: 1;
    width: 1000px;
    height: 100%;
`;
const next_button_css = css`
    left: 70%;
`;
const prev_button_css = css`
    right: 70%;
`;
const carousel_css = css`
    overflow: visible;
    .carousel-slider {
        overflow: visible;
    }
    .slider-wrapper {
        overflow: visible;
    }
    .slide {
        background-color: transparent;
    }
`;

const SlideContainer = styled.div`
    margin: auto;
    height: 100%;
`;

const Slide = styled.div`
    padding: 0 ${padding};
    pointer-events: none;
`;

const Text = styled.div`
    opacity: 0;
    &:not(:empty) {
        padding-top: 40px;
    }
    text-align: center;
    transition: opacity 0.5s;
    .__active & {
        opacity: 1;
    }
`;

export default CarouselComponent;
