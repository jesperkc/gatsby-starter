import { keyframes } from "@emotion/core";
import styled from "@emotion/styled";
import Image from 'gatsby-image';
import React from "react";
import Div100vh from 'react-div-100vh';
import { H2, H3 } from '../grid/typography.component';
import { Container, Row, Col, Spacer } from '../grid/grid.component';
import Header from '../header/header';

class HeroComponent extends React.Component {
  render() {

    const { category, title, descriptor, image, nav, lang, animateTitle = true } = this.props;

    return (

      <ImageHeroWrapper className={`image-hero-wrapper`}>
        <div className="clipper">
          <Header color="#fff" {...nav} lang={lang} />
        </div>
        <HeroText>
        <Container>
          <Row>
            <Col>
              
                {category && <H3 className="hero-anim">{category}</H3>}
                <H2 className={`${animateTitle ? 'hero-anim' : ''}`}>{title}</H2>
                {descriptor && <H3 className="hero-anim hero-descriptor">{descriptor}</H3>}
              
            </Col>
          </Row>
        </Container>
        </HeroText>
        <HeroImage>
          {image && <Image {...image.childImageSharp} loading={'eager'} />}
        </HeroImage>
      </ImageHeroWrapper>

    )
  }
}
export default HeroComponent


export const ImageHeroWrapper = styled(Div100vh)`
  position: relative;
  /* height: 100vh; */
  z-index: 2000;
  background-color: #f7f4ed;
}
`

const heroImageFade = keyframes`
  0% {
    transform: scale(1.2);
    opacity: 0;
  }
  30% {
    opacity: 1;
  }
  100% {
    transform: scale(1);
    opacity: 1;
  }
`
export const HeroImage = styled(Div100vh)`
  max-height: 100vh;
  overflow: hidden;

  .gatsby-image-wrapper{
    max-height: 100%;
    min-height: 100%;
    min-width: 100%;
    transition: transform 1000ms;
    will-change: transform;
    /* transform: scale(1); */
  }
  .tl-wrapper-status--entering &{
    .gatsby-image-wrapper{
      transition: transform 0ms;
      /* transform: scale(1); */
    }
  }

  .animate-from-refresh & {
    animation: ${heroImageFade} 2000ms ease-out forwards;
  }
`;

const heroTextFade = keyframes`
  0% {
    transform: translateY(30px);
    opacity: 0;
  }
  100% {
    transform: translateY(0px);
    opacity: 1;
  }
`
export const HeroText = styled.div`

  position: absolute;
  z-index: 20;
  color: ${props => props.color || '#fff'};
  left: 0;
  top: 50%;
  width: 100%;
  transform: translateY(-50%);

  h3{
    margin: 0;
    opacity: 1;
  }
  h2{
    margin: 1rem 0;
    white-space: pre-line;
    opacity: 1;
  }
  
  .hero-anim{
    opacity: 0;
  }
  


  h2.hero-anim, h3.hero-anim{
    animation: ${heroTextFade} 500ms ease-out forwards;
    animation-delay: 300ms;
  }

  .hero-anim ~ .hero-anim{
    animation-delay: 600ms;
  }
  .hero-anim ~ .hero-anim ~ .hero-anim{
    animation-delay: 900ms;
  }

`
