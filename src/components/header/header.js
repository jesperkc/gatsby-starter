import { css } from "@emotion/core";
import styled from "@emotion/styled";
import Link from 'gatsby-link';
import React from "react";
import Headroom from 'react-headroom';
import { ReactComponent as Logo } from '../../../static/images/logo.svg';
import { Container, Row, Col, Spacer } from '../grid/grid.component';
import { settings } from "../grid/settings";
const { slugs } = require("../../helpers/slugs");

class Header extends React.Component {

  constructor() {
    super();

    this.state = {
      isOpen: false
    }

    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
  }
  componentDidMount() {
    this.addEventListeners();
  }
  componentWillUnmount() {
    this.removeEventListeners();
  }
  open() {
    this.setState({ isOpen: !this.state.isOpen }, this.setBodyClass);
  }
  close() {
    this.setState({ isOpen: false }, this.setBodyClass);
  }
  setBodyClass() {
    if (this.state.isOpen) {
      document.body.classList.add('disableScroll');
    } else {
      document.body.classList.remove('disableScroll');
    }
  }
  addEventListeners() {
    window.addEventListener("resize", this.close, false);
  }
  removeEventListeners() {
    window.removeEventListener("resize", this.close, false);
  }
  render() {

    // const {
    //   nav_links,
    //   lang
    // } = this.props;

    const lang = 'en-gb';
    const nav_links = [
      {
        text: 'About',
        link: {uid: 'page-2'}
      },
      {
        text: 'Products',
        link: {uid: 'page-2'}
      }
    ]
    return (
      <Headroom disableInlineStyles>
        <HeaderWrapper color={this.props.color}>
          <Container className="container">
            <Row>
              <Col>
                <LogoWrapper><Link to={'/'}><Logo /></Link></LogoWrapper>
              </Col>
              <Col css={cell_right}>
                <MobileIcon onClick={this.open} color={this.props.color}>
                  <svg width='20' height='15' viewBox='0 0 20 15' xmlns='http://www.w3.org/2000/svg'>
                    <g fill='none' fillRule='evenodd'>
                      <g transform='translate(-325 -78)' fill='#000'>
                        <g transform='translate(325 78)'>
                          <rect width='20' height='1' />
                          <rect x='5' y='7' width='15' height='1' />
                          <rect y='14' width='20' height='1' />
                        </g>
                      </g>
                    </g>
                  </svg>
                </MobileIcon>
                <MenuWrapper className={this.state.isOpen ? 'is-open' : ''} onClick={this.close}>
                  <CloseIcon>
                    <svg width="16px" height="15px" viewBox="0 0 16 15" version="1.1" xmlns="http://www.w3.org/2000/svg">
                      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                        <g transform="translate(-329.000000, -79.000000)" fill="#000000">
                          <g transform="translate(329.000000, 79.000000)">
                            <rect transform="translate(8.000000, 7.424621) rotate(-315.000000) translate(-8.000000, -7.424621) " x="-2" y="6.9246212" width="20" height="1"></rect>
                            <rect transform="translate(8.000000, 7.424621) rotate(-45.000000) translate(-8.000000, -7.424621) " x="-2" y="6.9246212" width="20" height="1"></rect>
                          </g>
                        </g>
                      </g>
                    </svg>
                  </CloseIcon>
                  <ul>
                    {nav_links && nav_links.map(link => (
                      <li key={link.text}><Link to={slugs.nav(link.link && link.link.uid, lang)}><span>{link.text}</span></Link></li>
                    ))}
                  </ul>
                </MenuWrapper>
              </Col>
            </Row>
          </Container>
        </HeaderWrapper>
      </Headroom>
    )
  }

}
export default Header


const HeaderWrapper = styled.header`
  width: 100%;
  z-index: 1000;
  color: ${props => props.color || '#242423'};
  a{
    color: ${props => props.color || '#242423'};
    text-decoration: none;
    display: inline-block;
  }

  @media (min-width: ${settings.ipadWidth}px){
    a span:before {
      background-color: ${props => props.color || '#242423'};
    }
  }


  opacity: 1;
  transition: opacity 500ms;
  will-change: opacity;
  
  .tl-wrapper-status--entering & {
    transition: opacity 0ms;
    opacity: 0;
  }

  svg path{
    fill: ${props => props.color || '#242423'};
  }
`;

const cell_right = css`
  align-self: center;
  text-align: right;
`;

export const LogoWrapper = styled.div`
  text-transform: uppercase;
  a{
    padding: 37px 0 0 0;
    letter-spacing: 0.157rem;

    @media (max-width: ${settings.baseWidth - 1}px){
      padding-left: 0;
    }
  }

  svg{
    height: 30px;
  }
`;

const MobileIcon = styled.div`
  padding: 37px 0 0 0;
  @media (min-width: ${settings.ipadWidth}px){
    
    display: none;
  }
  g{
    fill: ${props => props.color || '#242423'};
  }
`;
const CloseIcon = styled.div`
  position: absolute;
  right: 12px;
  top: 0;
  padding: 40px;
  @media (min-width: ${settings.ipadWidth}px){
    
    display: none;
  }
`;
const MenuWrapper = styled.div`

  @media (max-width: ${settings.ipadWidth - 1}px){
    background-color: #f7f4ed;
    position: absolute;
    top: 0;
    left: 0;
    height: 100vh;
    width: 100vw;
    display: flex;
    transform: translateX(100%);
    transition: transform 200ms ease-out;
    a{
      color: #242423 !important;
    }
    &.is-open{
      transform: translateX(0%);
    }
  }
  
  ul{
    width: 100%;
    align-self: center;

    font-size: ${settings.typo.nav.max}px;
    line-height: ${settings.typo.nav.height};
    margin: 0;
    padding: 0;
    li{

      display: inline;
        
      &:last-of-type a{
        padding-right: 0;
        @media (min-width: ${settings.baseWidth}px){
          padding-right: 0;
        }
      }
    }
    a{
      padding: 37px 21px;
    }

    @media (max-width: ${settings.ipadWidth - 1}px){
      
      font-size: 22px;
      line-height: ${settings.typo.nav.height};

      a{
        padding: 17px 0;
        display: block;
        text-align: center;
      }
    }
  }
`;
