import React from 'react';
import Helmet from 'react-helmet';

class CustomHelmet extends React.Component {

  render() {
    let { title, description, image, url, twitterCard = 'summary_large_image' } = this.props;

    let seoImage = ''
    if (image === null) {
      image = seoImage;
    }
    // console.log('location', url);
    return (
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta name="image" content={image && image.url} />
        <link rel="canonical" href={url} />

        <meta name="pinterest" content="nopin" />
        <meta name="format-detection" content="telephone=no" />
        {/* OpenGraph tags */}
        <meta property="og:type" content="article" />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        {image && <meta property="og:image" content={image.url} />}
        {image && <meta property="og:image:width" content={image.width} />}
        {image && <meta property="og:image:height" content={image.height} />}
        <meta property="og:url" content={url} />

        {/* Twitter Card tags */}
        <meta name="twitter:card" content={twitterCard} />
        <meta name="twitter:title" content={title} />
        <meta name="twitter:description" content={description} />
        {image && <meta name="twitter:image" content={image.url} />}
        <meta name="twitter:site" content="@karimoku" />
        <meta name="twitter:creator" content="@karimoku" />
        <body className={''} />
      </Helmet>
    );
  }
}

export default CustomHelmet;
