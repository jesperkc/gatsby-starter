import styled from "@emotion/styled";
import { responsiveParam, responsiveWidth, settings, highlighter } from './settings';
import React from "react";

import {
  Container as ReactContainer,
  Row as ReactRow,
  Col as ReactCol
} from 'react-grid';

const styles = {
  gridBreakpoints: { xs: 0, sm: 576, md: 768, lg: 992, xl: 1200 },
  containerMaxWidths: { sm: 540, md: 720, lg: 960, xl: 1140 },
  gridColumns: 12,
  gridGutterWidth: 30
};

//export const Container = props => <ReactContainer {...props} styles={styles} />;
export const Row = props => <ReactRow {...props} styles={styles} style={{border: settings.debug ? '1px solid green' : ''}} />;
export const Col = props => <ReactCol {...props} styles={styles} style={{border: settings.debug ? '1px solid red' : ''}} />;



// Grid
export const OverflowHidden = styled("div")`
  overflow:hidden;
`;


export const Container = styled("div")`
/* border: 1px solid red; */

${responsiveParam('margin', settings.grid.containerMarginMin, settings.grid.containerMargin)}

${settings.scaleAboveDesktop === true ? `
` : `

@media (min-width: ${settings.grid.containerMaxWidth + (settings.baseWidth * (settings.grid.containerMargin / settings.baseWidth) * 2)}px){
  margin: 0 auto !important;
}
max-width: ${settings.grid.containerMaxWidth}px;
`}
  ${highlighter('blue')}
`;


export const ContainerScale = styled("div")`
  /* border: 1px solid red; */

  ${responsiveParam('margin', settings.grid.containerMarginMin, settings.grid.containerMargin)}
  
  
`;



// Row
// export const Row = styled("div")`
// ${responsiveParam('margin', -settings.grid.containerMarginMin, -settings.grid.containerMargin)}
// ${responsiveParam('padding', settings.grid.containerMarginMin, settings.grid.containerMargin)}
// display: flex;
// flex-wrap: ${props => props.wrap};


// @media (max-width: ${settings.mobileWidth - 1}px){
//   flex-wrap: wrap;
// }
// ${highlighter('red')}
// `;

export const BlockRow = styled("div")`
  ${responsiveParam('margin', -settings.grid.containerMarginMin, -settings.grid.containerMargin)}
  ${responsiveParam('padding', settings.grid.containerMarginMin, settings.grid.containerMargin)}

`;




export const Cell = styled("div")`
  /* min-height: 80px; */
  /* border: 1px solid green; */
  flex: 1 1 auto;

  ${highlighter('blue')}
`;

var i = 1;

for (i = 1; i < settings.grid.rows + 1; i++) {
  Cell[`x${i}`] = styled("div")`
  /* min-height: 80px;
  border: 1px solid green; */
  flex: 1 1 ${i * (100 / settings.grid.rows)}%;
  max-width: ${i * (100 / settings.grid.rows)}%;

  
  @media (max-width: ${settings.mobileWidth - 1}px){
    max-width: 100%;
    flex: 1 1 100%;
    /* min-height: 100px; */
  }
  ${highlighter('blue')}
`;
}

export const CellGutter = styled("div")`
  flex: 1 1 auto;
`;

for (i = 1; i < 4 + 1; i++) {
  CellGutter[`x${i}`] = styled("div")`
    ${responsiveWidth('width', settings.gutter.min * i, settings.gutter.max * i)}
  `;
}



export const Spacer100 = styled("div")`
  ${responsiveWidth('height', 50, 100)}
`;

export const Spacer300 = styled("div")`
  ${responsiveWidth('height', 150, 300)}
`;

export const Spacer = styled("div")`
  ${responsiveWidth('width', settings.spacers.min, settings.spacers.max)}
  ${responsiveWidth('height', settings.spacers.min, settings.spacers.max)}
  @media (max-width: ${props => props.break}){
    width: 100%;
  }
`;
for (i = 1; i < settings.spacers.count + 1; i++) {
  Spacer[`x${i}`] = styled("div")`
    ${responsiveWidth('width', settings.spacers.min * i, settings.spacers.max * i)}
    ${responsiveWidth('height', settings.spacers.min * i, settings.spacers.max * i)}

    @media (max-width: ${props => props.break}){
      width: 100%;
    }
  `;
}
