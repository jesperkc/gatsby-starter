const getSlugLang = (lang) => (lang === 'en-gb' ? '' : 'jp/');

const productSlug = (uid, lang, casestudy) => {
  return `/${getSlugLang(lang)}${casestudy ? casestudy.uid : 'demo'}/${uid}`
}

const navSlug = (uid, lang) => {
  return `/${getSlugLang(lang)}${uid}`
}

const slugs = {
  products: productSlug,
  nav: navSlug
}
// export default slugs;
module.exports.slugs = slugs;