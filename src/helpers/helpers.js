const caseStudyNumber = (case_study_number) => {
  return case_study_number.toString().length == 1 ? `0${case_study_number}` : case_study_number;
}
module.exports.caseStudyNumber = caseStudyNumber

const linkify = (inputText) => {
  var replacedText, replacePattern1, replacePattern2, replacePattern3;

  replacedText = inputText;
  // console.log('inputText', inputText);
  // //URLs starting with http://, https://, or ftp://
  // replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
  // replacedText = inputText.replace(replacePattern1, '[$1]($1)');

  // //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
  // replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
  // replacedText = replacedText.replace(replacePattern2, '[$1]($2)');

  //Change email addresses to mailto:: links.
  replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
  replacedText = replacedText.replace(replacePattern3, '[$1](mailto:$1)');

  return replacedText;
}
module.exports.linkify = linkify

const sortGrid = (products) => {
  return products.sort(function (a, b) {
    var textA = a.caseOrder;
    var textB = b.caseOrder;
    return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
  });
}

const orderGrid = (products) => {


  const sortedProducts = products;//sortGrid(products);
  // Sort by lang

  // for (var i = 0; i < sortedProducts.length; i++) {
  //     console.log('sortedProducts product ',sortedProducts[i].lang ,sortedProducts[i].data.title, sortedProducts[i].caseOrder);
  // }

  const wides = sortedProducts.filter(product => { return product.data.card_size === 'wide' });
  let normals = sortedProducts.filter(product => { return product.data.card_size === 'normal' });
  const cells = normals.length + (wides.length * 2);
  const rows = Math.round((cells / 3));

  const rowsWithWides = wides.length / rows;

  // console.log('cells', cells);
  // console.log('rows', rows);
  // console.log('rowsWithWides', rowsWithWides);
  // console.log('wides', wides);
  // console.log('normals', normals);

  let ordered_products = [];

  var order = true;

  for (var ii = 0; ii < wides.length; ii++) {
    if (order === true) {
      ordered_products.push(wides[ii]);
      if (normals.length) {
        ordered_products.push(normals[0]);
        normals.shift();
      }
    } else {
      if (normals.length) {
        ordered_products.push(normals[0]);
        normals.shift();
      }
      ordered_products.push(wides[ii]);
    }
    order = !order;

  }
  if (normals.length >= 3) {
    ordered_products.unshift(normals[0]);
    ordered_products.unshift(normals[1]);
    ordered_products.unshift(normals[2]);
    normals.shift();
    normals.shift();
    normals.shift();
  }
  for (var iii = 0; iii < normals.length; iii++) {
    ordered_products.push(normals[iii]);
  }
  for (var iii = 0; iii < ordered_products.length; iii++) {
    ordered_products[iii].data.case_study_number = iii;
  }

  // console.log('ordered_products', ordered_products);

  return ordered_products;
}

module.exports.orderGrid = orderGrid