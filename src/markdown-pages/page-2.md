---
path: "/page-2"
slug: "page-2"
lang: "en-gb"
date: "2019-05-04"
title: "My first blog post 2"
heroImage: ../images/hero.jpg
carousel:
  images:
    -
      image: ../images/hero.jpg
      title: "Image title"
    -
      image: ../images/square.jpg
      title: "Image title"
 
---