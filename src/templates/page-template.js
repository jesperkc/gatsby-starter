import { Link } from "gatsby";
import React from "react";
import HeroComponent from "../components/hero/hero.component";
import CarouselComponent from "../components/carousel/carousel.component";
import Layout from "../components/layout";
import SEO from "../components/seo";
import { Container, Row, Col, Spacer } from '../components/grid/grid.component';
import Image from "gatsby-image"

class PageTemplate extends React.Component {

  constructor() {
    super();

  }
  render() {
    const { data, transitionStatus, pageContext, location } = this.props;
    // console.log('location', location);
    // const animateFromTransition = location.state ? location.state.from == 'products' : false;
    console.log(this.props);
    const page = data ? data.page.frontmatter : null;
    // const tl = document.querySelector('.tl-edges > div');
    // console.log('Render page');
    return (
      <Layout>
        <SEO title="Home" />
        <HeroComponent {...{image: page.heroImage, title: 'Adidas new collection'}} />
          
          

<Spacer />
    <Container>
      <Row>
        <Col md={6}>
          <h1>{page && page.title}</h1>
        </Col>
        <Col md={6}>
          Half width
        </Col>
      </Row>
    </Container>
    <Spacer />
    <Container>
      <Row wrap={'wrap'}>
        <Col><h1>Grid</h1></Col>
        <Spacer.x1 break={'500px'}></Spacer.x1>
        <Col></Col>
      </Row>
    </Container>
    <Spacer />
    <Container>
      <Row>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
        <Col>.</Col>
      </Row>
    </Container>
    <Spacer />
    <CarouselComponent {...page.carousel}/>
    <Spacer />
    <Container>
      <h1>First line of H1<br />second line</h1>
      <h2>First line of H2<br />second line</h2>
      <h3>First line of H3<br />second line</h3>
      <h4>First line of H4<br />second line</h4>
      <h5>First line of H5<br />second line</h5>
      <h6>First line of H6<br />second line</h6>
      <p>I like to enter the base font-size for paragraph text. Then I select from the surrounding values for headers and small text.<br />
        There are no rules. Just experiment and have fun. Try using the values for line-height, margins or whatever, and see what works.</p>
      <p>There are no rules. Just experiment and have fun. Try using the values for line-height, margins or whatever, and see what works.</p>
      <input type="text" placeholder="placeholder" value="Text input" />
      <input type="checkbox" />
      <Link to="/page-2/">Go to page 2</Link>
      <Spacer.x3 />
    </Container>



      </Layout>
    )
  }
}

export default PageTemplate

export const query = graphql`
  query($path: String!) {
    page: markdownRemark(frontmatter: { path: { eq: $path } }) {
        frontmatter{
          title
          heroImage {
              childImageSharp {
                fluid(maxWidth: 800) {
                  ...GatsbyImageSharpFluid
                }
              }
          }
          carousel {
            images {
              title
              image {
                childImageSharp {
                  fluid(maxWidth: 800) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
    }
  }
`


PageTemplate.query = query;