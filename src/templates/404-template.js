import React from "react";
import FooterComponent from '../components/footer/footer';
import { Spacer } from "../components/grid/grid.component";

class NotFoundPage extends React.Component {

  constructor() {
    super();

  }

  render() {

    const { data, transitionStatus, pageContext } = this.props;
    return (

      <div>
        <Spacer />
        <FooterComponent {...data.footer.data} lang={data.footer.lang} />
      </div>
    )
  }
}
export default NotFoundPage;


// export const query = graphql`
//   query($lang: String!) {
//     nav: {
//     }
//     footer: {
//     }
//   }
// `


// NotFoundPage.query = query;