const { GraphQLString, GraphQLInt, GraphQLObjectType } = require('graphql');
const axios = require('axios');

const getTimeToRead = (file, args) => {
  let timeToRead = 1;
  if (file.modules___NODE) {
    for (var i = 0; i < file.modules___NODE.length; i++) {
      const node = args.getNode(file.modules___NODE[i]);
      if (node.textNode___NODE) {
        const textnode = args.getNode(node.textNode___NODE);
        const wordcount = textnode.text.split(' ').length;
        const readtime = wordcount / 200;

        //console.log('***', wordcount, ' = ', readtime);
        timeToRead += readtime;
      }
    }
  }
  return timeToRead;
};

module.exports = args => {
  const { type, store, pathPrefix, getNode, cache, reporter } = args;
  //324580
  // console.log(type);
  if (type.name === 'DatoCmsViewsNews') {
    return new Promise(resolve => {
      return resolve({
        timeToRead: {
          type: GraphQLInt,
          resolve(file) {
            const timeToRead = getTimeToRead(file, args);
            return Math.round(timeToRead);
          },
        },
      });
    });
  }

  if (type.name === 'DatoCmsAsset') {
    return new Promise(resolve => {
      return resolve({
        jsonData: {
          type: GraphQLString,
          async resolve(file) {
            let jsonData = '';
            if (file.format == 'json') {
              await axios.get(file.url).then(result => {
                jsonData = JSON.stringify(result.data);
                return result.data;
              });
            }
            return jsonData;
          },
        },
      });
    });
  }

  return {};
};
