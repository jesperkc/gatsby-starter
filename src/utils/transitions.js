import { css } from 'react-emotion';
import { Spring } from 'react-spring';


const defaultTransition = {
  cloneWrapperStyles: css``,
  navigationTiming: 0,
  transitionTiming: 0,
  animations() { },
  onBefore() { },
  onNavigationComplete() { },
  onTransitionComplete() { },
};

const getScrollOffset = () => {
  if (window.innerWidth > window.innerHeight) {
    return window.innerWidth * .065;
  } else {
    return window.innerHeight * .095;
  }
}

export default {
  toCase(stateClass) {
    return {
      ...defaultTransition,
      navigationTiming: 850,
      transitionTiming: 2500,
      animations(props) {
        return async next => {

          await next(Spring, {
            from: { width: props.width, height: props.height, top: props.top, left: props.left },
            to: props.width > props.height ? { width: window.innerWidth, height: props.height + 100, left: 0, top: props.top - 50 } : { height: window.innerHeight, width: props.width + 100, top: 0, left: props.left - 50 },
            config: { tension: 250, friction: 20, overshootClamping: true },
          })
          await next(Spring, {
            //from: props.width > props.height ? { height: props.height, top: props.top } : { width: props.width, left: props.left },
            to: props.width > props.height ? { height: window.innerHeight, top: 0 } : { width: window.innerWidth, left: 0 },
            config: { tension: 210, friction: 20, overshootClamping: true },
          })
          // await next(Spring, {
          //   from: { opacity: 1 },
          //   to: { opacity: 0 },
          //   config: { tension: 210, friction: 40, overshootClamping: true },
          // })
        };
      },
      onNavigationComplete() {
        window.scrollTo(0, getScrollOffset());
      },
    };
  },
  toNextCase(stateClass) {
    return {
      ...defaultTransition,
      navigationTiming: 550,
      transitionTiming: 1500,
      animations(props) {
        return async next => {

          await next(Spring, {
            from: { width: props.width, height: props.height, top: props.top, left: props.left },
            to: { height: window.innerHeight, top: 0 },
            config: { tension: 220, friction: 20, overshootClamping: true },
          })
          // await next(Spring, {
          //   from: { opacity: 1 },
          //   to: { opacity: 0 },
          //   config: { tension: 210, friction: 40, overshootClamping: true },
          // })
        };
      },
      onNavigationComplete() {
        //window.scrollTo(0, 70);
        window.scrollTo(0, getScrollOffset());
      },
    };
  },
  toPage(stateClass) {
    return {
      ...defaultTransition,
      cloneWrapperStyles: css`
                top: 0;
                bottom: 0;
                width: 100vw;
                overflow-y: scroll;
            `,
      navigationTiming: 0,
      transitionTiming: 700,
      animations() {
        return animation => {
          animation(Spring, {
            from: {
              opacity: 1,
            },
            to: {
              opacity: 0,
            },
          });
        };
      },
      onBefore() {
        // Scroll to correct place in clonewrapper
        document
          .getElementById('CloneWrapper')
          .scrollTo(0, window.pageYOffset);

        // Handle page leave
        stateClass.setState(() => ({ pageLeave: true }));
        setTimeout(() => {
          stateClass.setState(() => ({ pageLeave: false }));
        }, 300);
      },
      onNavigationComplete() {
        // Scroll to top
        window.scrollTo(0, 0);

        // Handle page enter
        stateClass.setState(() => ({ pageEnter: true }));
        setTimeout(() => {
          stateClass.setState(() => ({ pageEnter: false }));
        }, 1000);
      },
    };
  },
  toWork(stateClass) {
    return {
      ...defaultTransition,
      cloneWrapperStyles: css`
                top: 0;
                bottom: 0;
                width: 100vw;
                overflow-y: scroll;
            `,
      navigationTiming: 0,
      transitionTiming: 700,
      animations() {
        return animation => {
          animation(Spring, {
            from: {
              opacity: 1,
            },
            to: {
              opacity: 0,
            },
          });
        };
      },
      onBefore() {
        // Scroll to correct place in clonewrapper
        document
          .getElementById('CloneWrapper')
          .scrollTo(0, window.pageYOffset);

        // Handle page leave
        stateClass.setState(() => ({ pageLeave: true }));
        setTimeout(() => {
          stateClass.setState(() => ({ pageLeave: false }));
        }, 300);
      },
      onNavigationComplete() {
        // Scroll below top section
        window.scrollTo(0, window.innerHeight);

        // Handle page enter
        stateClass.setState(() => ({ pageEnter: true }));
        setTimeout(() => {
          stateClass.setState(() => ({ pageEnter: false }));
        }, 1000);
      },
    };
  },
};
