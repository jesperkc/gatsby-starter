import { graphql } from "gatsby";

// Images


export const ImageSharpTallSlimFragment = graphql`
fragment imageSharpTallSlimFragment on ImageSharp {
      fluid(maxWidth: 1000) {
        ...GatsbyImageSharpFluid_noBase64
      }
}
`

export const ImageSharpWideFragment = graphql`
fragment imageSharpWideFragment on ImageSharp {
      fluid(maxWidth: 1500) {
        ...GatsbyImageSharpFluid_noBase64
      }
}`

export const ImageSharpHeroFragment = graphql`
fragment imageSharpHeroFragment on ImageSharp {
      fluid(maxWidth: 3000) {
        ...GatsbyImageSharpFluid_noBase64
      }
}
`