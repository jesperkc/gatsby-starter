class MethodsService {
    constructor() {
        this.methods = {};
    }

    /**
     * @function addMethod
     * @description Adds method
     */
    addMethod(methodName, method) {
        this.methods = {
            ...this.methods,
            [methodName]: method,
        };
    }

    /**
     * @function getMethod
     * @description Getter for methods
     */
    getMethod(methodName) {
        return this.methods[methodName];
    }
}

export default new MethodsService();
