class DataStoreService {
  constructor() {
    this.cases = null;
    this.thoughts = null;
    this.menuOpen = false;
  }

  // Setters

  setMenuOpen(open) {
    this.menuOpen = open;
  }

  /**
   * @function setCases
   * @description Setter for cases
   */
  setCases(cases) {
    this.cases = cases;
  }

  /**
   * @function setThoughts
   * @description Setter for thoughts
   */
  setThoughts(thoughts) {
    this.thoughts = thoughts;
  }

  /**
   * @function setPeople
   * @description Setter for people
   */
  setPeople(people) {
    this.people = people;
  }

  /**
   * @function setPeople
   * @description Setter for people
   */
  setJobs(jobs) {
    this.jobs = jobs;
  }

  // Getters


  getMenuOpen() {
    return this.menuOpen;
  }

  /**
   * @function getCases
   * @description Getter for cases
   */
  getCases() {
    return this.cases;
  }

  /**
   * @function getThoughts
   * @description Getter for thoughts
   */
  getThoughts() {
    return this.thoughts;
  }

  /**
   * @function getPeople
   * @description Getter for people
   */
  getPeople() {
    return this.people;
  }

  /**
   * @function getPeople
   * @description Getter for people
   */
  getJobs() {
    return this.jobs;
  }
}

export default new DataStoreService();
